# mask grub from gentoo tree
sys-boot/grub:0::gentoo
sys-boot/grub:2::gentoo

# mask nvidia-drivers from gentoo tree
x11-drivers/nvidia-drivers::gentoo

# mask newer portage from gentoo tree due to entropy failures
>sys-apps/portage-2.2.14::gentoo

#mask genkernel,&& genkernel next from gentoo tree
sys-kernel/genkernel::gentoo
sys-kernel/genkernel-next::gentoo

# mask pinentry from gentoo tree, use splitted one from kogaion-desktop
app-crypt/pinentry::gentoo

# mask polkit-qt from gentoo tree, use splitted one from kogaion-desktop
sys-auth/polkit-qt::gentoo

# mask open-iscsi from our overlay, use gentoo one
sys-block/open-iscsi::kogaion

# mask plymouth from gentoo, use our own
sys-boot/plymouth::gentoo

# mask gentoo baselayout, user our own
sys-apps/baselayout::gentoo

# mask firefox from gentoo, use our own as it builds with PGO
www-client/firefox::gentoo

# mask gentoo gcc, use our splitted gcc
sys-devel/gcc::gentoo

# mask gentoo avahi, use our splitted avahi
net-dns/avahi::gentoo

# mask gentoo virtual/linux-sources, trying to hard to bring gentoo-sources in
virtual/linux-sources::gentoo

# mask mate-session-manager && mate-power-manager from gentoo, as they can link against upower-pm-utils, and we really really want to link only against upower since we're systemd only
# use our own ebuilds as they provide cherry picked upower1 api support, and they link only against upower
mate-base/mate-session-manager::gentoo
mate-extra/mate-power-manager::gentoo

# mascam driverele video din gentoo
x11-drivers/ati-userspace::gentoo
x11-drivers/ati-drivers::gentoo
x11-drivers/nvidia-drivers::gentoo
x11-drivers/nvidia-userspace::gentoo

#mask sys-fs/xfsprogs newer than 3.2.2 as it causes huge crashes and unbootable xfs systems with calamares see bug : https://calamares.io/bugs/browse/CAL-263
>sys-fs/xfsprogs-3.2.2
